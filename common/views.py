from django.views.generic.base import TemplateView


class TemplateTestView(TemplateView):
    template_name = 'common/internal_base.html'
