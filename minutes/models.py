from django.conf import settings
from django.db import models
from django.core.mail import send_mail
from django.contrib.auth.models import User

from markdownx.models import MarkdownxField
from markdownx.utils import markdownify
from parse import parse

from common.models import TimestampedModel
from contacts.models import Contact


STATUS_CHOICES = (
    ('todo', 'ToDo'),
    ('doing', 'Doing'),
    ('done', 'Done'),
)


class Minute(TimestampedModel):
    owner = models.ForeignKey('auth.User', related_name='minutes',  on_delete=models.CASCADE)
    title = models.CharField(max_length=120)
    notes = MarkdownxField()
    attendees_emails = models.TextField(blank=True, null=True)
    attendees_users = models.ManyToManyField('auth.User', related_name='minutes_attended', blank=True)

    def __str__(self):
        return '{title} | {owner}'.format(title=self.title, owner=self.owner.username)

    def __unicode__(self):
        return u'{}'.format(self.__str__())

    def send(self):
        name = self.owner.get_full_name() if self.owner.get_full_name() else self.owner.username
        subject = '{name} shared the minutes for {meeting_title}'.format(
            name=name,
            meeting_title=self.title,
        )
        body = self.notes
        recipients = self.attendees_emails.split(',')
        send_mail(
            subject=subject, 
            message=body, 
            from_email=settings.DEFAULT_FROM_EMAIL, 
            recipient_list=recipients,
            html_message=markdownify(body),
        )

    def save(self):
        attendees = [email.strip() for email in self.attendees_emails.split(',')]
        parse_format = '{name}<{email}>'
        plain_emails = []
        for email in attendees:
            result = parse(parse_format, email)
            if result:
                contact = Contact(owner=self.owner, name=result.named['name'], email=result.named['email'])
                plain_emails.append(result.named['email'])
            else:
                contact = Contact(owner=self.owner, email=email)
                plain_emails.append(email)
            contact.save()
        super().save()
        self.attendees_users.set(User.objects.filter(email__in=plain_emails))


class ActionItem(TimestampedModel):
    owner_email = models.EmailField()
    awner_user = models.ForeignKey('auth.User', related_name='action_items', on_delete=models.CASCADE)
    minute = models.ForeignKey(Minute, on_delete=models.CASCADE)
    title = models.CharField(max_length=120)
    status = models.CharField(max_length=10, choices=STATUS_CHOICES, default='todo')
    description = MarkdownxField(null=True, blank=True)

