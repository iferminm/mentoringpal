from django.views.generic.base import TemplateView
from django.views.generic.edit import FormView
from django.shortcuts import get_object_or_404
from django.urls import reverse_lazy

from minutes.models import Minute
from minutes import forms


class MinuteList(TemplateView):
    template_name = 'minutes/minute_list.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['minutes'] = Minute.objects.filter(owner=self.request.user).order_by('-updated_at')
        return context


class MinuteView(TemplateView):
    template_name = 'minutes/minute_view.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        minute_id = kwargs['minute_id']
        context['minute'] = get_object_or_404(Minute, pk=minute_id)
        return context


class MinuteCreate(FormView):
    template_name = 'minutes/minute_create.html'
    form_class = forms.MinuteForm
    success_url = reverse_lazy('minutes-list')

    def form_valid(self, form):
        minute = form.save(commit=False)
        minute.owner = self.request.user
        minute.save()
        minute.send()
        return super().form_valid(form)

