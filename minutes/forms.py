from django import forms

from markdownx.fields import MarkdownxFormField

from minutes.models import Minute
from minutes.models import ActionItem


class MinuteForm(forms.ModelForm):
    notes = MarkdownxFormField()

    class Meta:
        model = Minute
        exclude = ('created_at', 'updated_at', 'attendees_users', 'owner')


class ActionItemForm(forms.ModelForm):
    class Meta:
        model = ActionItem
        exclude = ('created_at', 'updated_at')
