from django.contrib import admin
from minutes.models import Minute
from minutes.models import ActionItem


admin.site.register(Minute)
admin.site.register(ActionItem)
