from django.contrib.auth.decorators import login_required
from django.urls import path

from minutes.views import MinuteList
from minutes.views import MinuteView
from minutes.views import MinuteCreate


urlpatterns = [
    path('list/', login_required(MinuteList.as_view()), name='minutes-list'),
    path('<int:minute_id>/detail/', login_required(MinuteView.as_view()), name='minutes-detail'),
    path('create/', login_required(MinuteCreate.as_view()), name='minutes-create'),
]
