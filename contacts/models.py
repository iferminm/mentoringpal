from django.db import models
from django.contrib.auth.models import User

from common.models import TimestampedModel


class Contact(TimestampedModel):
    owner = models.ForeignKey('auth.User', related_name='agenda', on_delete=models.CASCADE)
    name = models.CharField(max_length=120, null=True, blank=True)
    email = models.EmailField()
    user = models.ForeignKey('auth.User', on_delete=models.CASCADE, blank=True, null=True)
    callsign = models.CharField(max_length=120)

    def __str__(self):
        return '{name}<{email}> | {owner}\' agenda'.format(
            name=self.name,
            email=self.email,
            owner=self.owner.username,
        )

    def __unicode__(self):
        return u'{}'.format(self.__str__())

    def save(self):
        user = User.objects.filter(email=self.email)
        if user.exists():
            self.user = user.get()
        if not self.callsign:
            self.callsign = self.email.split('@')[0]

        super().save()
