from django.db import models

from common.models import TimestampedModel


class UserProfile(TimestampedModel):
    user = models.OneToOneField('auth.User', on_delete=models.CASCADE)
    country = models.CharField(max_length=256)
    location = models.CharField(max_length=256)
    timezone = models.CharField(max_length=256)

    def __str__(self):
        return '{user} | {email}'.format(user=self.user.username, email=self.user.email)

    def __unicode__(self):
        return u'{}'.format(self.__str__())


