from django.contrib import admin

from useraccounts.models import UserProfile


admin.site.register(UserProfile)
